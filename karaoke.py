
#!/usr/bin/python3
# -*- coding: utf-8 -*-

from xml.sax import make_parser
from xml.sax.handler import ContentHandler
from smallsmilhandler import SmallSMILHandler
from urllib.request import urlretrieve
import sys
import json


class KaraokeLocal():

    def __init__(self):
        parser = make_parser()
        handler = SmallSMILHandler()
        parser.setContentHandler(handler)
        parser.parse(open(smil_file))
        self.atribute_list = handler.get_tags()

    def __str__(self):

        for tags_dicc in self.atribute_list:
            line = ""
            tag = tags_dicc['Tag']
            # print(tag)
            line += tag

            for atribute in tags_dicc:
                value = tags_dicc[atribute]
                if value != "":
                    line += '\t' + atribute + "=" + value
            print(line)

    def do_json(self, file):

        filename = open(file[:-5] + '.json', 'w')
        json_file = json.dumps(self.atribute_list)
        filename.write(json_file)

    def do_local(self):

        for tags_dicc in self.atribute_list:
            for atribute in tags_dicc:
                # print(atribute)
                if atribute == "src" and tags_dicc[atribute][:7] == "http://":
                    # print(tags_dicc[atribute])
                    a = tags_dicc[atribute].split('/')
                    last = a[-1]
                    # print(a[-1])
                    urlretrieve(tags_dicc[atribute], last)
                    tags_dicc[atribute] = last
                    # print(tags_dicc[atribute])


if __name__ == "__main__":

    try:
        smil_file = sys.argv[1]
    except:
        sys.exit("Usage: Python karaoke.py file.smil")

    kk = KaraokeLocal()
    kk.__str__()
    kk.do_json(smil_file)
    kk.do_local()
    local_json_file = 'local.json'
    kk.do_json(local_json_file)
    kk.__str__()
